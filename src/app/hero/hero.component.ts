import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

    @Input()
    paragrapth1: string=''

    @Input()
    paragrapth2: string=''

    @Input()
    paragrapth3: string=''

  constructor() { }

  ngOnInit(): void {
  }

}
