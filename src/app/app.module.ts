import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrimeroComponent } from './primero/primero.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TitlesComponent } from './titles/titles.component';
import { HeroTopComponent } from './hero-top/hero-top.component';
import { HeroComponent } from './hero/hero.component';
import { TrinityItemsComponent } from './trinity-items/trinity-items.component';
import { ParagraphsComponent } from './paragraphs/paragraphs.component';
import { ArticleComponent } from './article/article.component';
import { ItemViewComponent } from './item-view/item-view.component';

@NgModule({
  declarations: [
    AppComponent,
    PrimeroComponent,
    HeaderComponent,
    FooterComponent,
    TitlesComponent,
    HeroTopComponent,
    HeroComponent,
    TrinityItemsComponent,
    ParagraphsComponent,
    ArticleComponent,
    ItemViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
