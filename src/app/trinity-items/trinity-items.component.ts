import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-trinity-items',
  templateUrl: './trinity-items.component.html',
  styleUrls: ['./trinity-items.component.css']
})
export class TrinityItemsComponent implements OnInit {

  @Input()
  text1: string="Xbox";
  image1: string="https://i.blogs.es/6b8fd3/xbox-orig/1366_2000.jpg";

  @Input()
  text2: string="Playstation 2";
  image2: string="https://as01.epimg.net/meristation/imagenes/2015/12/04/noticia/1449248400_549417_1532442420_sumario_normal.jpg";
  
  @Input()
  text3: string="Gamecube";
  image3: string="https://m.media-amazon.com/images/I/61fCEvJAG0L._AC_SX450_.jpg";

  constructor() { }

  ngOnInit(): void {
  }

}

